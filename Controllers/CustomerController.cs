﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace WebAPIProject.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {     
       

        // GET: api/Customer
        [HttpGet]
        public Object Get()
        {
            using (StreamReader r = new StreamReader("myjson.json"))
            {
                string myjson = r.ReadToEnd();
                var items = JsonConvert.DeserializeObject<Customers>(myjson);
                return items;
            }

        }
        // POST: api/Customer
        
        [HttpPost]
        public Object Post([FromBody] string value)
        {
            var customer = JsonConvert.DeserializeObject<CustomerData>(value);
            Customers jsonObject;
            using (StreamReader r = new StreamReader("myjson.json"))
            {
                string myjson = r.ReadToEnd();
                jsonObject = JsonConvert.DeserializeObject<Customers>(myjson);
                jsonObject.customers.Add(customer);
            }
            string json = JsonConvert.SerializeObject(jsonObject);
            System.IO.File.WriteAllText(@"myjson.json", json);
            return customer;

        }

        // PUT: api/Customer/5
        [HttpPut("{id}")]
        public CustomerData Put(int id, [FromBody] string value)
        {
            var updated = false;
            Customers jsonObject;
            var customerUpdated = JsonConvert.DeserializeObject<CustomerData>(value);
            using (StreamReader r = new StreamReader("myjson.json"))
            {
                string myjson = r.ReadToEnd();
                jsonObject = JsonConvert.DeserializeObject<Customers>(myjson);
                var customer = jsonObject.customers.Find(x => x.PersonId == id);
                if (customer != null)
                {
                    customer.FirstName = customerUpdated.FirstName;
                    customer.LastName = customerUpdated.LastName;
                    customer.City = customerUpdated.City;
                    updated = true;
                }
            }
            if (updated )
            {
                string json = JsonConvert.SerializeObject(jsonObject);
                System.IO.File.WriteAllText(@"myjson.json", json);
                return customerUpdated;
            }
            return null;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var deleted = false;
            Customers jsonObject;
            using (StreamReader r = new StreamReader("myjson.json"))
            {
                string myjson = r.ReadToEnd();
                jsonObject = JsonConvert.DeserializeObject<Customers>(myjson);
                var customer= jsonObject.customers.Find(x => x.PersonId == id);
                if(customer != null)
                    deleted = jsonObject.customers.Remove(customer);
            }
            string json = JsonConvert.SerializeObject(jsonObject);
            System.IO.File.WriteAllText(@"myjson.json", json);
            return deleted;
        }
    }
}